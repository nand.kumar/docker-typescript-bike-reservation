import { DataSource } from "typeorm"

export const connectToDb = new DataSource({
    type: "mysql",
    port:3306,
    host: process.env.MYSQL_HOST,
    username: 'root',
    password: process.env.MYSQL_ROOT_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    entities: ["src/entities/**/*.ts"],
    logging: true,
    synchronize: true,
})